using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public string filepath;
    public Data data;

    public void LoadDataTwitch()
    {
        BinaryFormatter bf = new BinaryFormatter(); // el formateador binario
        if (File.Exists(filepath)) // si el archivo existe
        {
            FileStream file = File.Open(filepath, FileMode.Open); //que abra el archivo 
            Data dataLoaded = (Data)bf.Deserialize(file);
            data = dataLoaded;
            file.Close();
            Debug.Log("Se cargaron los Datos");
        }
    }

    public void SaveDataTwitch(string pName, string pToken, string pChannel)
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filepath);
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Se guardaron los Datos");
    }

    [System.Serializable] // para verlo en el inspector
    public class Data
    {

        public Data()
        {

        }

    }
}
