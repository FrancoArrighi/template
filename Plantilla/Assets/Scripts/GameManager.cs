using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public delegate void StartGameDelegate();
    public StartGameDelegate StartGameDel;

    public delegate void FinishGameDelegate();
    public FinishGameDelegate FinishGameDel;

    public delegate void PauseGameDelegate();
    public PauseGameDelegate PauseGameDel;


    #region Singleton
    public static GameManager instance { get; private set; }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    #endregion

    public void FinishGame()
    {
        FinishGameDel();
    }
}
