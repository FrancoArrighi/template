using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinamicEntity : MonoBehaviour
{

    public Transform startPos;
    public Transform _transform { get; set; }

    public Rigidbody _rb;

    public DinamicEntity()
    {
        if(GetComponent<Rigidbody>() != null)
        _rb = GetComponent<Rigidbody>();

        _transform = transform;
        _transform.position = startPos.position;

        GameManager.instance.FinishGameDel += ResetEntityValues;
    }

    public void ResetEntityValues()
    {
        _transform.position = startPos.position;
    }
}
